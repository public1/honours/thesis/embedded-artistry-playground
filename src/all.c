#include <assert.h>
#include <stdbool.h>
#include <malloc.h>

#include <Project/projectMain.h>

char heap[4 * 1024 * 1024] = {0};

// Tell the compiler incoming stack alignment is not RSP%16==8 or ESP%16==12
__attribute__((force_align_arg_pointer))
void _start() {

    /* main body of program: call main(), etc */
	assert(true);
	malloc_init();
	malloc_addblock((void *) heap, 4 * 1024 * 1024);

    int *allocated = (int *) malloc(1 * sizeof(int));
    // assert((uintptr_t) allocated != 0);

    projectMain();

    /* exit system call */
    asm("movl $1,%eax;"
        "xorl %ebx,%ebx;"
        "int  $0x80"
    );
    __builtin_unreachable();  // tell the compiler to make sure side effects are done before the asm statement
}

