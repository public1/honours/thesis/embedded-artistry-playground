PWD_DIR = "$(shell basename $$(pwd))"
SRC_DIR = $(PWD)/src
BUILD_DIR = $(PWD)/buildresults

LIBC_SUBMODULE = $(PWD)/libc
LIBMEMORY_SUBMODULE = $(PWD)/libmemory
LIBCPP_SUBMODULE = $(PWD)/libcpp

LIBC_BUILDRESULTS = $(LIBC_SUBMODULE)/buildresults
LIBMEMORY_BUILDRESULTS = $(LIBMEMORY_SUBMODULE)/buildresults
LIBCPP_BUILDRESULTS = $(LIBCPP_SUBMODULE)/buildresults

LIBC_INCLUDES = $(LIBC_SUBMODULE)/include
LIBMEMORY_INCLUDES = $(LIBMEMORY_SUBMODULE)/include
LIBCPP_INCLUDES = $(LIBCPP_SUBMODULE)/include

SERVER_USER_HOST = patrick@vm_comp4961_ubuntu1804
SERVER_REMOTE_DIR = ~/remote/$(shell hostname -s)/

.PHONY: directories
directories:
	mkdir -p $(BUILD_DIR)

# =================================
# Push to remote servers.
# =================================

.PHONY: push-home
push-home:
	# Make the directory on the remote server if it doesn't exist already.
	ssh -t $(SERVER_USER_HOST) "mkdir -p $(SERVER_REMOTE_DIR)$(PWD_DIR)"
	# Sync our current directory with the remote.
	rsync -a \
 			--exclude "buildresults" \
 			--delete \
 			./ $(SERVER_USER_HOST):$(SERVER_REMOTE_DIR)$(PWD_DIR)

# ==================================
# Runs a Make command remotely.
# ==================================

.PHONY: remote
remote: push-home
	ssh -t $(SERVER_USER_HOST) "\
		cd $(SERVER_REMOTE_DIR)$(PWD_DIR) ; \
		zsh -ilc 'make $(MAKE_CMD)' ; "

# ==================================
# Clean
# ==================================

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)/*

# ==================================
# Debug
# ==================================

.PHONY: debug-ld-libc
debug-ld-libc:
	ld -L$(LIBC_BUILDRESULTS)/src -lc --verbose

.PHONY: debug-ld-libmemory
debug-ld-libmemory:
	ld -L$(LIBMEMORY_BUILDRESULTS)/src -lmemory_freelist --verbose

.PHONY: debug-ld-libcpp
debug-ld-libcpp:
	ld -L$(LIBCPP_BUILDRESULTS) -lc++ -lc++abi --verbose

# ==================================
# Build
# ==================================

.PHONY: build-libc
build-libc:
ifeq ("$(wildcard $(LIBC_BUILDRESULTS))","")
	cd $(LIBC_SUBMODULE) && git submodule update --init --recursive
	cd $(LIBC_SUBMODULE) && $(MAKE)
else
	@echo "No need to build submodule since it already exists."
endif


.PHONY: build-libmemory
build-libmemory:
ifeq ("$(wildcard $(LIBMEMORY_BUILDRESULTS))","")
	cd $(LIBMEMORY_SUBMODULE) && git submodule update --init --recursive
	cd $(LIBMEMORY_SUBMODULE) && $(MAKE)
else
	@echo "No need to build submodule since it already exists."
endif

.PHONY: build-libcpp
build-libcpp:
ifeq ("$(wildcard $(LIBCPP_BUILDRESULTS))","")
	cd $(LIBCPP_SUBMODULE) && git submodule update --init --recursive
	cd $(LIBCPP_SUBMODULE) && $(MAKE)
else
	@echo "No need to build submodule since it already exists."
endif

.PHONY: build
build: \
	build-libc \
	build-libmemory \
	build-libcpp \
	directories
	gcc \
		-nostdlib \
		-ffreestanding \
		$(SRC_DIR)/all.c \
		$(SRC_DIR)/Project/projectMain.cpp \
		-o $(BUILD_DIR)/all \
		-L$(LIBC_BUILDRESULTS)/src/ -lc \
		-L$(LIBMEMORY_BUILDRESULTS)/src/ -lmemory_freelist \
		-L$(LIBCPP_BUILDRESULTS)/ -lc++ -lc++abi \
		-I$(LIBC_INCLUDES) \
		-I$(LIBMEMORY_INCLUDES) \
		-I$(LIBCPP_INCLUDES) \
		-I$(LIBC_SUBMODULE)/arch/x86_64/include \
		-I$(SRC_DIR)/

# ==================================
# Run
# ==================================

.PHONY: run
run: clean build
	$(BUILD_DIR)/all

